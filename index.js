// console.log("Hello World!");

// [SECTION] Functions
// Functions in javascript are lines/blocks of codes that tell our application to perfrm a certain task when called/invoked

/* 
    Syntax:
            function functionName() {
                code block (statement)
            }
*/

// function keyword - used to define a js functions
// functionName - we set name so we can use it for later
// function block ({}) - this is where code to be executed

function printName() {
    console.log("My name is John");
}

printName(); //Invocation - calling a function that needs to be executed

function declaredFunction() {
    console.log("Hello world from declaredFunction()");
}
declaredFunction();

// Function Expression
// a function can also be stored in a variable. THis is called a Function Expression

let variableFunction = function(){
    console.log("Hello Again!");
}

variableFunction();

let funcExpression = function funcName(){
    console.log("Hello from the other side");
}

funcExpression();

funcExpression = function(){
    console.log("Updated funcExpression");
}

funcExpression();

// re-assigning declaredFunction() value;

declaredFunction = function(){
    console.log("Updated declaredFunction");
}

declaredFunction();

// Re-assigning function declared with const

const constantFunc = function(){
    console.log("Initialize with const!");
}

constantFunc();

// Re-aasignment of a const function

// constantFunc = function() {
    // console.log("Can we re-assign it?");
// }

// constantFunc();

// [SECTION] Function Scoping

{
    let localVar = "Armando Perez";
    console.log(localVar);
}

// console.log(localVar); --> error because the data of localVar is "locally stored" on it's function only

// console.log(globalScope); --> error since the console was used even before the globalScope was declared

let globalScope = "Mr. Worldwide";
console.log(globalScope);

// Function Scoping

function showNames() {
    // Function scope variables
    const functionConst = "John";
    let functionLet = "Jane";

    console.log(functionConst);
    console.log(functionLet);
}

showNames();
// console.log(functionConst); --> cannot be used because the variables are located inside the scope of a function
// console.log(functionLet);

// Nested Function
// a function inside a function

function myNewFunction(){
    let name = "Jane";

    function nestedFunction(){
        let nestedName = "John";
        console.log(name);
    }
    nestedFunction();
}

myNewFunction();
// nestedFunction(); --> will cause error because the function is located within a function scoped parent function


// Functiona nd Global scope variable

// Global scope variable

let globalName = "Alexandro";

function myNewFunction2() {
    let nameInside = "Renz";

    console.log(globalName);
}

myNewFunction2();
// console.log(nameInside); 

// [SECTION] Using alert()
// it shows a small window at the top of our browser that show information to our users.

alert("Hello World!"); //this will be executed immediately

function showAlert() {
    alert("Hello user!");
}

showAlert();

console.log("I will only log in the console when alert is this dismissed");

// [SECTION] using prompt()
// shos us a small window at the top of the browser to gather input

let samplePrompt = prompt("Enter your Name.");

// console.log("Hello " + samplePrompt);
// console.log(typeof samplePrompt);

// prompt() can be used to gather user input
// prompt() can be run immediately like alert()

function printWelcomeMessage() {
    let firstName = prompt("Enter your first name.");
    let lastName = prompt("Enter your last name.");
    console.log("Hello " + firstName + " " + lastName + "!");
    console.log("Welcome to my page!");
}

printWelcomeMessage();

// [SECTION] Function naming convention
// function names should be definitive of the task it will perform. It should contain a verb.

function getCourse() {
    let courses = ["Science 101", "Math 101", "English 101"];
    console.log(courses);
}

getCourse();

// Avoid generic names to avoid confusion within your code.

function get() {
    let name = "Jamie";
    console.log(name);
}

get();
// Avoid pointless and inappropriate function names

function foo() {
    console.log(25%5);
}

foo();

// Name your functions in small caps. Follow camelCase when naming functions.

// camelCase --> nameShawn
// snake_case --> name_shawn
// kebab-case --> name-shawn

function displayCarInfo() {
    console.log("Brand: Toyota");
    console.log("Type: Sedan");
    console.log("Price: 1, 500, 000");
}

displayCarInfo();